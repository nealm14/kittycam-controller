package nm.org.kitcamctrl;

/*
 * KittyCam Controller
 * A Simple Android controller app for
 * KittyCam A Raspberry Pi app using a camera PIR motion sensor, with cat facial detection
 *
 * Neal Marriott
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ItemDatabasehandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "buggycam.db";
    private static String DB_PATH = "";

    // ITEMS table name
    private static final String TABLE_ITEMS = "rpi";

    // ITEMS Table Columns names
    private static final String KEY_ID = "itemID";
    private static final String KEY_IP = "hostname";
    private static final String KEY_USR = "user";
    private static final String KEY_PW = "passwd";
    private static final String KEY_PORT = "port";
    private static final String KEY_FRN = "frn"; //Friendly Name eg BuggyCam Home
    private SQLiteDatabase mDataBase;

    public ItemDatabasehandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ITEMS_TABLE = "CREATE TABLE " + TABLE_ITEMS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_IP + " TEXT,"
                + KEY_USR + " TEXT," + KEY_PW + " TEXT," + KEY_PORT + " TEXT," + KEY_FRN + " TEXT" + ")";
        db.execSQL(CREATE_ITEMS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEMS);

        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new ItemRecord
    void addItemRecord(ItemRecord ItemRecord) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_IP, ItemRecord.getHostname()); // ItemRecord Name
        values.put(KEY_USR, ItemRecord.getUser()); // ItemRecord Phone
        values.put(KEY_PW, ItemRecord.getPasswd()); // ItemRecord Name
        values.put(KEY_PORT, ItemRecord.getPort()); // ItemRecord Phone
        values.put(KEY_FRN, ItemRecord.getFRN()); // ItemRecord Name

        // Inserting Row
        db.insert(TABLE_ITEMS, null, values);
        db.close(); // Closing database connection
    }

    // Getting single ItemRecord
    ItemRecord getItemRecord(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_ITEMS, new String[]{KEY_ID,
                        KEY_IP, KEY_USR,KEY_PW,KEY_PORT,KEY_FRN}, KEY_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null
        );
        if (cursor != null)
            cursor.moveToFirst();

        ItemRecord ItemRecord = new ItemRecord(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));
        // return ItemRecord
        return ItemRecord;
    }

    // Getting All ITEMS
    public List<ItemRecord> getAllItemRecords() {
        List<ItemRecord> ItemRecordList = new ArrayList<ItemRecord>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ITEMS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                ItemRecord item = new ItemRecord();
                item.setID(Integer.parseInt(cursor.getString(0)));
                item.setHostname(cursor.getString(1));
                item.setUser(cursor.getString(2));
                item.setPasswd(cursor.getString(3));
                item.setPort(cursor.getString(4));
                item.setFRN(cursor.getString(5));
                // Adding ItemRecord to list
                ItemRecordList.add(item);
            } while (cursor.moveToNext());
        }

        // return ItemRecord list
        return ItemRecordList;
    }

    // Updating single ItemRecord
    public int updateItem(ItemRecord item) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_IP, item.getHostname());
        values.put(KEY_USR, item.getUser());
        values.put(KEY_PW, item.getPasswd());
        values.put(KEY_PORT, item.getPort());
        values.put(KEY_FRN, item.getFRN());

        // updating row
        return db.update(TABLE_ITEMS, values, KEY_ID + " = ?",
                new String[]{String.valueOf(item.getID())});
    }

    // Deleting single ItemRecord
    public void deleteItem(ItemRecord item) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_ITEMS, KEY_ID + " = ?",
                new String[]{String.valueOf(item.getID())});
        db.close();
    }


    // Getting ITEMS Count
    public int getItemCount() {
        String countQuery = "SELECT  * FROM " + TABLE_ITEMS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


    //Check that the database exists here: /data/data/your package/databases/Da Name
    private boolean checkDataBase()
    {
        File dbFile = new File(DB_PATH + DATABASE_NAME);
        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
        return dbFile.exists();
    }

    @Override
    public synchronized void close()
    {
        if(mDataBase != null)
            mDataBase.close();
        super.close();
    }

}