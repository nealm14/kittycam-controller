package nm.org.kitcamctrl;

/*
 * KittyCam Controller
 * A Simple Android controller app for
 * KittyCam A Raspberry Pi app using a camera PIR motion sensor, with cat facial detection
 *
 * Neal Marriott
 */

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AboutMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AboutMenuFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;


    TextView cpLink;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AboutMenuFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AboutMenuFragment newInstance(long id) {
        AboutMenuFragment fragment = new AboutMenuFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        args.putLong("id", id);
        fragment.setArguments(args);

        return fragment;
    }

    public AboutMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_menu, container, false);
                cpLink = (TextView) view.findViewById(R.id.about_cpLink);

        cpLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("http://cats.org.uk");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(uri, "text/html"); //video/mp4
                startActivity(intent);
            }
        });

        // Inflate the layout for this fragment
        setRetainInstance(true);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("id", 4);
    }



    public void onPause() {
        super.onPause();
        Bundle b = new Bundle();
        b.putInt("id", 4);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //NOT REALLY NEEDED
        super.onConfigurationChanged(newConfig);
        Bundle b = new Bundle();
        b.putInt("id", 4);
    }

}
