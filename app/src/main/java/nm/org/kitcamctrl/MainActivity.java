package nm.org.kitcamctrl;

/*
 * KittyCam Controller
 * A Simple Android controller app for
 * KittyCam A Raspberry Pi app using a camera PIR motion sensor, with cat facial detection
 *
 * Neal Marriott
 */

import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;


import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int id = 0;
        try {
            id = savedInstanceState.getInt("id");
        } catch (NullPointerException e) {

        }
        android.support.v4.app.FragmentTransaction transaction;
        setContentView(R.layout.activity_main);
        FacebookSdk.sdkInitialize(getApplicationContext());
        FacebookSdk.setApplicationId(getResources().getString(R.string.facebook_app_id));
        callbackManager = CallbackManager.Factory.create();
        if (id ==0 ){

            MainActivityFragment maFragment = MainActivityFragment.newInstance(id);
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frag_container, maFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbarCall();
        }
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "nm.org.kitcamctrl",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }

    @Override
    public void onBackPressed() {
            DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                super.onBackPressed();
            }
    }

    public boolean isFBLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if( id == R.id.nav_home) {
            fragmentSwitch(0);
        }
        else if (id == R.id.nav_ssh_control) {
            fragmentSwitch(1);
            // Handle the camera action

        } else if(id == R.id.nav_hostlist) {
            fragmentSwitch(2);


// Commit the transaction


        }

        else if (id == R.id.nav_about) {
        fragmentSwitch(4);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    void fragmentSwitch(int fSwitch) {
        android.support.v4.app.FragmentTransaction transaction;
        int id;
        switch (fSwitch) {

            case 0:
                id = 0;
                MainActivityFragment maFragment = MainActivityFragment.newInstance(id);
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, maFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;
            case 1:
                id = 1;
                AddCameraFragment addCameraFragment = AddCameraFragment.newInstance(id);
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, addCameraFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                toolbarCall();
                break;

            case 2:
                id = 2;
                CameraListFragment cameraListFragment = CameraListFragment.newInstance(id);
                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, cameraListFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                toolbarCall();
                break;
            case 3:
                id = 3;
               CameraControlFragment cameraControlFragment = CameraControlFragment.newInstance(null, null, null,0,id);
                transaction = getSupportFragmentManager().beginTransaction(); //was getSupportFragmentManager
                //transaction.
                transaction.replace(R.id.frag_container, cameraControlFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                toolbarCall();
            case 4:
                id = 4;
                AboutMenuFragment aboutMenuFragment = AboutMenuFragment.newInstance(id);

                transaction = getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frag_container, aboutMenuFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                toolbarCall();
                break;
        }

    }
    public void toolbarCall() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        setContentView(R.layout.activity_main);
        toolbarCall();
        Bundle b = new Bundle();
        int id = b.getInt("id");
        Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.frag_container);

        android.support.v4.app.FragmentTransaction transaction;
        if (currentFragment instanceof AddCameraFragment) {
            AddCameraFragment addCameraFragment = AddCameraFragment.newInstance(id);
            transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.frag_container, addCameraFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbarCall();
        }
        super.onConfigurationChanged(newConfig);

    }

    public void onResume() {
        super.onResume();
        Bundle b = new Bundle();
        int id = b.getInt("id");

    }

}
