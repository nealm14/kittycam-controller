package nm.org.kitcamctrl;

/*
 * KittyCam Controller
 * A Simple Android controller app for
 * KittyCam A Raspberry Pi app using a camera PIR motion sensor, with cat facial detection
 *
 * Neal Marriott
 */

public class ItemRecord {

    //private variables
    int _id;
    String _hostname;
    String _user;
    String _passwd;
    String _port;
    String _frn;

    // Empty constructor
    public ItemRecord() {

    }

    // constructor
    public ItemRecord(int id, String hostname, String user ,String passwd, String port, String frn) {
        this._id = id;
        this._hostname = hostname;
        this._user = user;
        this._passwd = passwd;
        this._port = port;
        this._frn = frn;
    }

    // constructor
    public ItemRecord(String hostname, String user, String passwd, String port, String frn) {
        this._hostname = hostname;
        this._user = user;
        this._passwd = passwd;
        this._port = port;
        this._frn = frn;
    }

    // getting ID
    public int getID() {
        return this._id;
    }

    // setting id
    public void setID(int id) {
        this._id = id;
    }

    // getting name
    public String getHostname() {
        return this._hostname;
    }

    // setting name
    public void setHostname(String hostname) {
        this._hostname = hostname;
    }

    public String getUser() {
        return this._user;
    }

    public void setUser(String user) {
        this._user = user;
    }

    public String getPasswd() {
        return this._passwd;
    }

    public void setPasswd(String passwd) {
        this._passwd = passwd;
    }

    public String getPort() {
        return this._port;
    }

    public void setPort(String port) {
        this._port = port;
    }

    public String getFRN() {
        return this._frn;
    }

    public void setFRN(String frn) {
        this._frn = frn;
    }
}