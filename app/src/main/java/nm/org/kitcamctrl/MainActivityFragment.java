package nm.org.kitcamctrl;

/*
 * KittyCam Controller
 * A Simple Android controller app for
 * KittyCam A Raspberry Pi app using a camera PIR motion sensor, with cat facial detection
 *
 * Neal Marriott
 */

import android.content.Intent;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


//FACEBOOK
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;


/**
 * A placeholder fragment containing a simple view.
 */
public class MainActivityFragment extends Fragment {

    LoginButton loginButton;
    CallbackManager callbackManager;
    TextView getAccessTokenLBL;
    TextView accessTokenDSP;
    String LOGTAG = "KITTYCAM MAIN";
    private int id;


    // TODO: Rename and change types of parameters
    public static MainActivityFragment newInstance(int id) {
        MainActivityFragment fragment = new MainActivityFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        args.putInt("id",id);
        fragment.setArguments(args);
        return fragment;
    }

    public MainActivityFragment() {
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_main, container, false);
        accessTokenDSP = (TextView) view.findViewById(R.id.main_accessTokenDSP);
        getAccessTokenLBL = (TextView) view.findViewById(R.id.main_ATLBL);
        getAccessTokenLBL.setVisibility(View.INVISIBLE);
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();

        File dataFileDir = Environment.getDataDirectory();

        if(isFBLoggedIn()) {
            AccessToken getAccessToken = (AccessToken.getCurrentAccessToken());
            getAccessToken.getToken();
            getAccessTokenLBL.setVisibility(View.VISIBLE);
            try {
                readyKittyCam(getAccessToken.getToken().toString());
            } catch (IOException e) {
                e.printStackTrace();
            }

        }


        loginButton = (LoginButton) view.findViewById(R.id.main_authFBButton);
        loginButton.setPublishPermissions("publish_actions");
        // If using in a fragment
        loginButton.setFragment(this);
        // Other app specific specialization
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code

                AccessToken getAccessToken = (AccessToken.getCurrentAccessToken());
                getAccessToken.getToken();
                Log.d(LOGTAG, "FACEBOOK SUCCESS");
                if (!getAccessToken.getToken().isEmpty()) {
                    getAccessTokenLBL.setVisibility(View.VISIBLE);
                    accessTokenDSP.setText(R.string.main_accessTokenAlert);
                }
            }

            @Override
            public void onCancel() {
                getAccessTokenLBL.setVisibility(View.INVISIBLE);
                accessTokenDSP.setText("");
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });

        return view;

    }

    void readyKittyCam(String AccessToken) throws IOException {
        //Read Stock KittyCam.js into Buffer & Add ACCESS_TOKEN using regEX

        AssetManager am = getActivity().getAssets();
        String dataDir = getActivity().getFilesDir()+getString(R.string.main_datadir).toString();
        Log.d(LOGTAG, dataDir);
        File dataFileDir = new File(dataDir);

        if(!dataFileDir.exists()) {
            dataFileDir.mkdir();
        }

        InputStream is = am.open("data/kittyCam.js");
        File outFile = new File(dataFileDir, "kittyCam.js");

        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        PrintWriter pw = new PrintWriter(new FileWriter(outFile));

        String line = null;

        // Read from the original file and write to the new
        // unless content matches data to be removed.
        while ((line = br.readLine()) != null) {


            String re1 = "(INASTOK)";    // Word 1
            String lineToRemove = "var ACCESS_TOKEN = \"INASTOK\"";
            String AccessTokenVar = "var ACCESS_TOKEN = "+AccessToken;

            if (!line.trim().contains(lineToRemove)) {
                pw.println(line);
                pw.flush();
            }
            if(line.trim().contains(lineToRemove)) {
                pw.println(line.replaceAll(re1,AccessToken));
                pw.flush();
            }
        }

        pw.close();
        br.close();

        accessTokenDSP.setText(R.string.main_accessTokenAlert);

    }


    public boolean isFBLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        return accessToken != null;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("id", 0);
    }
    public void onPause() {
        super.onPause();
        Bundle b = new Bundle();
        b.putInt("id", 0);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //NOT REALLY NEEDED
        super.onConfigurationChanged(newConfig);
        Bundle b = new Bundle();
        b.putInt("id", 0);
    }

}

