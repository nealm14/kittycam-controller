package nm.org.kitcamctrl;

/*
 * KittyCam Controller
 * A Simple Android controller app for
 * KittyCam A Raspberry Pi app using a camera PIR motion sensor, with cat facial detection
 *
 * Neal Marriott
 */

import android.content.ClipData;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.os.Bundle;
//import android.app.Fragment;
import android.support.v4.app.Fragment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nm.org.kitcamctrl.listcontent.ListAdapterContent;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Large screen devices (such as tablets) are supported by replacing the ListView
 * with a GridView.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class CameraListFragment extends Fragment implements AbsListView.OnItemClickListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * The fragment's ListView/GridView.
     */
    private AbsListView mListView;

    /**
     * The Adapter which will be used to populate the ListView/GridView with
     * Views.
     */
    private BaseAdapter mAdapter;

    //DATABASE
    private final Handler handler = new Handler();
    private ArrayList<HashMap<String, String>> itemArrayList;
    HashMap<String, String> map;
    HashMap<String, String> frnMap;
    ItemDatabasehandler db;
    public static Context c;

    // TODO: Rename and change types of parameters
    public static CameraListFragment newInstance(int id) {
        CameraListFragment fragment = new CameraListFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        args.putInt("id",id);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public CameraListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        db = new ItemDatabasehandler(getActivity());



        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        // TODO: Change Adapter to display your content
        loadList();

        }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);
       // getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        loadList();
        // Set the adapter
        mListView = (AbsListView) view.findViewById(android.R.id.list);
        ((AdapterView<ListAdapter>) mListView).setAdapter(mAdapter);

        // Set OnItemClickListener so we can be notified on item clicks
        mListView.setOnItemClickListener(this);

        //setRetainInstance(true);
        return view;
    }
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, final long id) {

        AlertDialog.Builder alert = new AlertDialog.Builder(
                getActivity());
        alert.setTitle(getString(R.string.camlistfrag_alerttitle));
        alert.setMessage(getString(R.string.camlistfrag_alertmessage));
        alert.setNegativeButton(getString(R.string.camlistfrag_alertneg),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        //DELETE CAMERA FUNCTION
                        String id = itemArrayList.get(position).get("ITEM_ID");
                        try {
                            ItemRecord item = db.getItemRecord(Integer.valueOf(id));
                            db.deleteItem(item);

                        } catch (NullPointerException e) {
                            Log.d("DELETE CAM", "NPX");
                        }
                        // TODO Auto-generated method stub
                        mAdapter.notifyDataSetChanged();
                        loadList();


                    }
                }
        );
        alert.setPositiveButton(getString(R.string.camlistfrag_alertpos),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        //START OPERATION FRAGMENT
                        String hostname = itemArrayList.get(position).get("ITEM_IP");
                        String user = itemArrayList.get(position).get("ITEM_USER");
                        String passwd = itemArrayList.get(position).get("ITEM_PW");
                        int port = Integer.valueOf(itemArrayList.get(position).get("ITEM_PORT"));
                        CameraControlFragment cameraControlFragment = CameraControlFragment.newInstance(hostname, user, passwd, port, 3);
                        android.support.v4.app.FragmentTransaction transaction = getFragmentManager().beginTransaction(); //was getSupportFragmentManager
                        //transaction.
                        transaction.replace(R.id.frag_container, cameraControlFragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
        );
        alert.show();

        if (null != mListener) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mListener.onFragmentInteraction(ListAdapterContent.ITEMS.get(position).id);



        }
    }

    /**
     * The default content for this Fragment has a TextView that is shown when
     * the list is empty. If you would like to change the text, call this method
     * to supply the text it should use.
     */
    public void setEmptyText(CharSequence emptyText) {
        View emptyView = mListView.getEmptyView();

        if (emptyView instanceof TextView) {
            ((TextView) emptyView).setText(emptyText);
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

    void loadList() {
        itemArrayList = new ArrayList<HashMap<String, String>>();
        List<ItemRecord> values = db.getAllItemRecords();
        int value;
        for (ItemRecord cn : values) {
            String ITEM_ID = Integer.toString(cn.getID());
            Log.d("ITEM ID: ", ITEM_ID);
            // creating new HashMap
            map = new HashMap<String, String>();
            // adding each child node to HashMap key => value

            map.put("ITEM_ID", ITEM_ID);
            map.put("ITEM_IP", cn.getHostname());
            map.put("ITEM_USER", cn.getUser());
            map.put("ITEM_PW", cn.getPasswd());
            map.put("ITEM_PORT", cn.getPort());
            map.put("ITEM_FRN", cn.getFRN());
            // adding HashList to ArrayList
            itemArrayList.add(map);
        }

        //mAdapter = new ArrayAdapter<itemArrayList>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS);
        c = getActivity().getApplicationContext();

        String frn;
        ArrayList frnArray;
        frnArray = new ArrayList<ClipData.Item>();
        for (int i = 0; i < itemArrayList.size(); i ++) {
            frnMap = new HashMap<String, String>();
            // adding each child node to HashMap key => value



            frnArray.add(itemArrayList.get(i).get("ITEM_FRN"));
        }

        mAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1, android.R.id.text1,frnArray);
        mAdapter.notifyDataSetChanged();
        db.close();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("id", 2);
        mAdapter.notifyDataSetChanged();
        loadList();
    }
    public void onPause() {
        super.onPause();
        Bundle b = new Bundle();
        b.putInt("id", 2);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        //NOT REALLY NEEDED
        super.onConfigurationChanged(newConfig);
        Bundle b = new Bundle();
        b.putInt("id", 2);
    }

}
