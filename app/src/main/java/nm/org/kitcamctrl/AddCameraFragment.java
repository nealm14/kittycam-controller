package nm.org.kitcamctrl;

/*
 * KittyCam Controller
 * A Simple Android controller app for
 * KittyCam A Raspberry Pi app using a camera PIR motion sensor, with cat facial detection
 *
 * Neal Marriott
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Properties;



public class AddCameraFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddCameraFragment.
     */
    // TODO: Rename and change types and number of parameters


    public static String ITEM_ID; //DATABASE RECORD ID REQUIRED FOR LIST
    private final Handler handler = new Handler();
    private ItemDatabasehandler db;
    private Button connectToHost;
    private Button testHostConnection;
    private Button editPostMessage;
    private ImageButton addToFavourites;
    private TextView addToFavLabel;
    private EditText ipAddressEntry;
    private EditText userEntry;
    private EditText pwEntry1;
    private EditText pwEntry2;
    private EditText portEntry;
    private EditText FRNentry;
    private int port;
    private boolean kittycamTrans; //boolean for completed kittycam transfer
    private int connectionSuccess;
    private boolean scpSuccess;

    public static AddCameraFragment newInstance(int id) {
        AddCameraFragment fragment = new AddCameraFragment();
        Bundle args = new Bundle();
        //args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        args.putInt("id", id);
        fragment.setArguments(args);
        return fragment;
    }

    public AddCameraFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new ItemDatabasehandler(getActivity());
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sshcontrol, container, false);
        db = new ItemDatabasehandler(getActivity());
        addToFavourites = (ImageButton) view.findViewById(R.id.sshctrl_addfav);
        addToFavLabel = (TextView) view.findViewById(R.id.sshctrl_addfavLbl);
        testHostConnection = (Button) view.findViewById(R.id.sshctrl_testConnectbutton);
        connectToHost = (Button) view.findViewById(R.id.sshctrl_connectButton);
        editPostMessage = (Button) view.findViewById(R.id.sshctrl_editmsg);
        ipAddressEntry = (EditText) view.findViewById(R.id.sshctrl_entryip);
        userEntry = (EditText) view.findViewById(R.id.sshctrl_userentry);
        pwEntry1 = (EditText) view.findViewById(R.id.sshctrl_pwentry1);
        portEntry = (EditText) view.findViewById(R.id.sshctrl_portentry);
        FRNentry = (EditText) view.findViewById(R.id.sshctrl_frnentry);

        addToFavourites.setVisibility(View.GONE);
        addToFavLabel.setVisibility(View.GONE);
        connectToHost.setVisibility(View.GONE);

    addToFavourites.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            final String ipAddress = ipAddressEntry.getText().toString();
            final String user = userEntry.getText().toString();
            final String pswd = pwEntry1.getText().toString();
            String portString = portEntry.getText().toString();
            String frnString = FRNentry.getText().toString();
            if (!pswd.isEmpty() && !ipAddress.isEmpty() && !user.isEmpty() && !portString.isEmpty()) {
                db.addItemRecord(new ItemRecord(ipAddress, user, pswd, portString,frnString));
                db.close();
                ipAddressEntry.setText("");
                userEntry.setText("");
                pwEntry1.setText("");
                portEntry.setText("");
                FRNentry.setText("");
                addToFavLabel.setVisibility(View.INVISIBLE);
                addToFavourites.setVisibility(View.INVISIBLE);
                connectToHost.setVisibility(View.INVISIBLE);
            }

        }
    });



        connectToHost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final String ipAddress = ipAddressEntry.getText().toString();
                final String user = userEntry.getText().toString();
                final String pswd = pwEntry1.getText().toString();
                String portString = portEntry.getText().toString();
                final int port = Integer.valueOf(portEntry.getText().toString());
                if (!pswd.isEmpty() && !ipAddress.isEmpty() && !user.isEmpty() && port > 0) {

                    new AsyncTask<Integer, Void, Void>(){
                        @Override
                        protected Void doInBackground(Integer... params) {
                            try {
                                initiateSCP(ipAddress, user, pswd, port);

                            } catch (Exception e) {
                                e.printStackTrace();
                                Log.d("CHN", "177");
                            }
                            return null;
                        }
                    }.execute(1);
                    if (scpSuccess = true) {
                        addToFavLabel.setVisibility(View.VISIBLE);
                        addToFavourites.setVisibility(View.VISIBLE);
                    }

                }
                else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            getActivity());
                    alert.setTitle(getString(R.string.sshcredentials_dataalertTitle));
                    alert.setMessage(getString(R.string.sshcredentials_dataalertMessage));
                    alert.setPositiveButton(getString(R.string.sshcredentials_dataalertPOS),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                }
                            }
                    );
                    alert.show();

                }


            }
        });


        testHostConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String ipAddress = ipAddressEntry.getText().toString();
                final String user = userEntry.getText().toString();
                final String pswd = pwEntry1.getText().toString();
                String portString = portEntry.getText().toString();

                try {
                    port = Integer.valueOf(portEntry.getText().toString());
                } catch (NullPointerException e) {
                }


                if (!pswd.isEmpty() && !ipAddress.isEmpty() && !user.isEmpty() && port > 0) {

                    new AsyncTask<Integer, Void, Void>() {
                        @Override
                        protected Void doInBackground(Integer... params) {
                            try {
                                testConnectionToHost(ipAddress, user, pswd, port);


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            return null;
                        }

                    }.execute(1);
                }
                Log.d("CS VAlue", String.valueOf(connectionSuccess));
                if (connectionSuccess == 1) {
                    connectToHost.setVisibility(View.VISIBLE);
                }
                if (connectionSuccess == 0) {
                    Log.d("CS", " inside false");
                    AlertDialog.Builder alert = new AlertDialog.Builder(
                            getActivity());
                    alert.setTitle(getString(R.string.sshcredentials_cTestalertTitle));
                    alert.setMessage(getString(R.string.sshcredentials_cTestalertMessage));
                    alert.setPositiveButton(getString(R.string.sshcredentials_cTestalertPOS),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int whichButton) {
                                }
                            }
                    );
                    alert.show();
                }

            }
        });



        editPostMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                final Context context = alert.getContext();
                LinearLayout alertLayout = new LinearLayout(context);
                alertLayout.setOrientation(LinearLayout.VERTICAL);
                alert.setTitle(R.string.sshcredentials_editpostmsgTitle);

                final EditText titleInput = new EditText(context);
                titleInput.setHint(context.getString(R.string.sshcredentials_editpostmsgMessage));
                //titleInput.setText(.getText().toString());
                alertLayout.addView(titleInput);

                alert.setView(alertLayout);

                alert.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton) {
                        String postMessage;
                        try {
                            postMessage = titleInput.getText().toString();
                            try {
                                editKCPostMsg(postMessage);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        } catch (NullPointerException e) {
                            Log.d("NO POST MSG", "TRUE");
                        }

                    }
                });
                alert.show();
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    void  editKCPostMsg(String postMessage) throws IOException {

        String dataDir = getActivity().getFilesDir()+getString(R.string.main_datadir).toString();
        File dataFileDir = new File(dataDir);

        if(!dataFileDir.exists()) {
            dataFileDir.mkdir();
        }

        File inFile = new File(dataFileDir,"kittyCam.js");
        FileInputStream is = new FileInputStream(inFile);

        File outFile = new File(dataFileDir, "kittyCam2.js");

        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        PrintWriter pw = new PrintWriter(new FileWriter(outFile));

        String line = null;

        // Read from the original file and write to the new
        // unless content matches data to be removed.
        while ((line = br.readLine()) != null) {



            String re1 = "(\"Meow\")";	//"(INASTOK)";    // Word 1
            String lineToRemove = "form.append('message', \"Meow\")";//"var ACCESS_TOKEN = \"INASTOK\"";
            String asmPostMsg = "\""+postMessage+"\"";

            if (!line.trim().contains(lineToRemove)) {
                pw.println(line);
                pw.flush();
            }
            if(line.trim().contains(lineToRemove)) {
                pw.println(line.replaceAll(re1,asmPostMsg));
                pw.flush();
            }
        }

        pw.close();
        br.close();
        inFile.delete();
        if(!inFile.exists()) {
            File rnamed = new File(dataFileDir, "kittyCam.js");
            outFile.renameTo(rnamed);
        }


    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    void validatePasswords() {
        String m1 = pwEntry1.getText().toString();
        String m2 = pwEntry2.getText().toString();
        if(m1.isEmpty()&&m2.isEmpty()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(
                    getActivity());
            alert.setTitle(getString(R.string.sshcredentials_pwalertTitle));
            alert.setMessage(getString(R.string.sshcredentials_pwalertMessage));
            alert.setPositiveButton(getString(R.string.sshcredentials_pwalertPOS),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int whichButton) {
                        }
                    }
            );
            alert.show();
        }
        else {
            if (m1.contains(m2)) {
              testHostConnection.setVisibility(View.VISIBLE);
            }
        }

    }


    void testConnectionToHost(String hostname, String user, String pswd, int port) { //throws IOException {

        //int cSuccess = 0;

        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(user, hostname, port);
        } catch (JSchException e) {
            e.printStackTrace();
        }
        session.setPassword(pswd);
        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        try {
            session.connect();

        } catch (JSchException e) {
            connectionSuccess = 0;
            Log.d("TEST CONNECTION", "JSCHEXP");
            e.printStackTrace();
        }
        if (session.isConnected()) {
            Log.d("TEST CONNECTION", "connected");
            connectionSuccess = 1;
            session.disconnect();
        }

        //return cSuccess;

    }

    private File dataPath(String s) {
        File f = new File(s);
        return f;
    }

    String initiateSCP(String hostname, String user, String pswd, int port) {
        FileInputStream fis = null;
        //Data file path
        String dataDir = getActivity().getFilesDir()+getString(R.string.main_datadir) +"/kittyCam.js".toString();
        String rFile = "kittycam/kittyCam.js"; //name for remote file

        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(user, hostname, port);
        } catch (JSchException e) {
            e.printStackTrace();
        }
        session.setPassword(pswd);
        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        try {
            session.connect();
        } catch (JSchException e) {
            e.printStackTrace();
        }

        // SSH Channel
        ChannelExec channelssh = null;
        try {
            channelssh = (ChannelExec)
                    session.openChannel("exec");
        } catch (JSchException e) {
            e.printStackTrace();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        channelssh.setOutputStream(baos);


        boolean pts = false; //bool option for scp -p preserve timestamp switch
        String command="scp " + (pts ? "-p" :"") +" -t "+rFile;
        channelssh.setCommand(command);
        try {
            channelssh.connect();
        } catch (JSchException e) {
            e.printStackTrace();

        }

        //Open edited kittyCam.js file from /data/data/$PACKAGENAME/files/fb/

        //IO Steam  declarations
        InputStream is;
        OutputStream os;
        File dataFile = new File(dataDir);
        try {
            is = channelssh.getInputStream();
             os = channelssh.getOutputStream();
            if(pts) {
                command = "T " + (dataFile.lastModified() / 1000) + " 0";
                // The access time should be sent here,
                // but it is not accessible with JavaAPI ;-<
                command += (" " + (dataFile.lastModified() / 1000) + " 0\n");
                os.write(command.getBytes());
                os.flush();
                if (checkAck(is) != 0) {
                }
            }


            // send "C0644 filesize filename", where filename should not include '/'
            long filesize=dataFile.length();
            command="C0644 "+filesize+" ";
            if(dataDir.lastIndexOf('/')>0){
                command+=dataDir.substring(dataDir.lastIndexOf('/')+1);
            }
            else{
                command+=dataDir;
            }
            command+="\n";
            os.write(command.getBytes()); os.flush();
            if(checkAck(is)!=0){
            }

            // send a content of lfile
           fis = new FileInputStream(dataDir);
            byte[] buf=new byte[1024];
            while(true){
                int len=fis.read(buf, 0, buf.length);
                if(len<=0) break;
                os.write(buf, 0, len); //out.flush();
            }
            fis.close();
            fis=null;
            // send '\0'
            buf[0]=0; os.write(buf, 0, 1); os.flush();
            if(checkAck(is)!=0){
                //System.exit(0);
            }
            os.close();

            channelssh.disconnect();
            session.disconnect();
            scpSuccess = true;
            //System.exit(0);
        }
        catch(Exception e){
            System.out.println(e);
            try{if(fis!=null)fis.close();}catch(Exception ee){}
        }
        return  baos.toString();
    }

    static int checkAck(InputStream is) throws IOException{
        int b=is.read();
        // b may be 0 for success,
        //          1 for error,
        //          2 for fatal error,
        //          -1
        if(b==0) return b;
        if(b==-1) return b;

        if(b==1 || b==2){
            StringBuffer sb=new StringBuffer();
            int c;
            do {
                c=is.read();
                sb.append((char)c);
            }
            while(c!='\n');
            if(b==1){ // error
                System.out.print(sb.toString());
            }
            if(b==2){ // fatal error
                System.out.print(sb.toString());
            }
        }
        return b;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("id", 1);
    }

    public void onPause() {
        super.onPause();
        Bundle b = new Bundle();
        b.putInt("id", 1);
    }

    @Override
    public void onStop() {
        super.onStop();
        addToFavourites.setVisibility(View.INVISIBLE);
        addToFavLabel.setVisibility(View.INVISIBLE);
        connectToHost.setVisibility(View.INVISIBLE);

    }

   @Override
    public void onConfigurationChanged(Configuration newConfig) {
       //NOT REALLY NEEDED
       Bundle b = new Bundle();
       b.putInt("id", 1);
       super.onConfigurationChanged(newConfig);

    }
}
