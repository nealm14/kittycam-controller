package nm.org.kitcamctrl;

/*
 * KittyCam Controller
 * A Simple Android controller app for
 * KittyCam A Raspberry Pi app using a camera PIR motion sensor, with cat facial detection
 *
 * Neal Marriott
 */

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CameraControlFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CameraControlFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CameraControlFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private static final String ARG_HOST = "host";
    private static final String ARG_USER = "user";
    private static final String ARG_PASSWD = "passwd";
    private static final String ARG_PORT = "port";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private String host;
    private String user;
    private String passwd;
    private int port;

    private String statusOutput;
    boolean stopRequested;
    boolean runShutDownCmd;
    boolean kittyCamRunning;
    ChannelExec channelssh;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddCameraFragment.
     */
    // TODO: Rename and change types and number of parameters


    public static String ITEM_ID; //DATABASE RECORD ID REQUIRED FOR LIST
    private final Handler handler = new Handler();
    ImageButton startCamera;
    ImageButton stopCamera;
    ImageButton haltCamera;
    public static TextView statusDSP;
    TextViewUpdater textViewUpdater = new TextViewUpdater();
    Handler textViewUpdaterHandler = new Handler(Looper.getMainLooper());


    public static CameraControlFragment newInstance(String host, String user, String passwd, int port, long id) {
        CameraControlFragment fragment = new CameraControlFragment();
        Bundle args = new Bundle();
        args.putString(ARG_HOST, host);
        args.putString(ARG_USER, user);
        args.putString(ARG_PASSWD, passwd);
        args.putInt(ARG_PORT, port);
        args.putLong("id", id);
        fragment.setArguments(args);
        return fragment;
    }

    public CameraControlFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            host = getArguments().getString(ARG_HOST);
            user = getArguments().getString(ARG_USER);
            passwd = getArguments().getString(ARG_PASSWD);
            port = getArguments().getInt(ARG_PORT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_camera_control, container, false);
        startCamera = (ImageButton) view.findViewById(R.id.ffc_startkcbutton);
        stopCamera = (ImageButton) view.findViewById(R.id.ffc_stopkcbutton);
        haltCamera = (ImageButton) view.findViewById(R.id.ffc_haltcamerabutton);
        statusDSP = (TextView) view.findViewById(R.id.ffc_statusText);


        haltCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog.Builder alert = new AlertDialog.Builder(
                        getActivity());
                alert.setTitle(getString(R.string.fccstring_haltAlertTitle));
                alert.setMessage(getString(R.string.fccstring_haltAlertMsg));
                alert.setPositiveButton(getString(R.string.fccstring_haltAlertPos),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                shutDownCmd();
                            }
                        });
                alert.setNegativeButton(getString(R.string.fccstring_haltAlertNeg),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int whichButton) {
                                dialog.dismiss();
                            }
                        });
                alert.show();
            }
        });


        stopCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                stopRequested = true;
                Log.d("STOP", "PRESSED");
                //ASYNC TASK WAS HERE
            }
        });


        startCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AsyncTask<Integer, Void, Void>() {
                    @Override
                    protected Void doInBackground(Integer... params) {
                        try {
                            String command = "cd kittycam && sudo node kittyCam.js"; //"./runkittycam.sh"; ;

                            kittyCamRunning = runCameraCmd(host, user, passwd, port, command);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        return null;
                    }

                }.execute(1);

            }
        });

        setRetainInstance(true);
        // Inflate the layout for this fragment
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }


    boolean runCameraCmd(String hostname, String user, String pswd, int port, String command) {
        boolean cmStatus = false;
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(user, hostname, port);
        } catch (JSchException e) {
            e.printStackTrace();
        }
        session.setPassword(pswd);
        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        try {
            session.connect();
        } catch (JSchException e) {
            e.printStackTrace();
        }

        // SSH Channel
        channelssh = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        //ByteArrayInputStream bais = null;
        try {
            channelssh = (ChannelExec) session.openChannel("exec");
            // channelssh.setInputStream(bais);

        } catch (JSchException e) {
            e.printStackTrace();
        }
        channelssh.setOutputStream(baos);
        //  channelssh.setCommand("cd kittycam");
        channelssh.setCommand(command);


        int data = 0;
        InputStream input = null;
        StringBuffer buffer = new StringBuffer();
        CharArrayWriter outputBuffer = null;
        try {
//            Looper.prepare();

            channelssh.connect();

            InputStream in = channelssh.getInputStream();
            //This is the recommended way to read the output
            byte[] tmp = new byte[1024];
            while (true) {
                while (in.available() > 0) {
                    int i = in.read(tmp, 0, 1024);
                    if (i < 0) {
                        break;
                    }
                    buffer.append(new String(tmp, 0, i));
                }
                if (channelssh.isClosed()) {
                    //channelStatus = false;
                    stopRequested = false;
                    System.out.println("exit-status: " + channelssh.getExitStatus());
                    break;
                }
                if (channelssh.isConnected()) {
                    cmStatus = true;
                    System.out.println("connection status: " + channelssh.getExitStatus());
                    System.out.print(buffer.toString());
                    ///pulls the status of kittycam js Live to system outpu
                    textViewUpdater.setText(buffer.toString());
                    textViewUpdaterHandler.post(textViewUpdater);

                    if (stopRequested == true) {
                        try {
                            //channelssh.sendSignal("9");
                            // channelssh.sendSignal("2");
                            channelssh.disconnect();
                            session.disconnect();
                            stopRequested = false;
                            kittyCamRunning = false;
                            textViewUpdater.setText(getString(R.string.fccstring_kittycamClosed));
                            textViewUpdaterHandler.post(textViewUpdater);

                            //if(runShutDownCmd =true) {
                                //shutDownCmd();
                            //}
                        } catch (Exception e) {
                            Log.d("SIG EXP", e.toString());
                            e.printStackTrace();
                        }
                    }

                    // break;
                }
                try {
                    Thread.sleep(1000);
                } catch (Exception ee) {
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSchException e) {
            e.printStackTrace();
        } catch (NullPointerException e1) {
            Log.d("", "SSH NPE");
        }

        List<String> sysOutput = new ArrayList<String>();

        Scanner scanner = new Scanner(buffer.toString());
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            sysOutput.add(line);
        }

        try {
        } catch (NullPointerException e) {
        }
        return cmStatus; //Integer.toString(data);
    }


    void runCameraCmdNoLogging(String hostname, String user, String pswd, int port, String command) {
        JSch jsch = new JSch();
        Session session = null;
        try {
            session = jsch.getSession(user, hostname, port);
        } catch (JSchException e) {
            e.printStackTrace();
        }
        session.setPassword(pswd);
        // Avoid asking for key confirmation
        Properties prop = new Properties();
        prop.put("StrictHostKeyChecking", "no");
        session.setConfig(prop);

        try {
            session.connect();
        } catch (JSchException e) {
            e.printStackTrace();
        }

        // SSH Channel
       ChannelExec haltCam = null;
        try {
            haltCam = (ChannelExec) session.openChannel("exec");

        } catch (JSchException e) {
            e.printStackTrace();
        }
        haltCam.setCommand("sudo halt");
        try {
            haltCam.connect();
            if (haltCam.isConnected()) {

                System.out.println("connection status: " + haltCam.getExitStatus());
                //System.out.print(buffer.toString());
                ///pulls the status of kittycam js Live to system outpu
                textViewUpdater.setText("CAMERA SHUTDOWN");
                textViewUpdaterHandler.post(textViewUpdater);
            }

        } catch (JSchException e) {
            e.printStackTrace();
        }

    }


    void shutDownCmd() {
        stopRequested = true;

        //if (kittyCamRunning = false) {
            new AsyncTask<Integer, Void, Void>() {
                @Override
                protected Void doInBackground(Integer... params) {
                    try {
                        String command = "sudo shutdown -h now";
                        runCameraCmdNoLogging(host, user, passwd, port, command);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    return null;
                }

            }.execute(1);
        //}
    }


    class TextViewUpdater implements Runnable {
        private String txt;

        @Override
        public void run() {
            CameraControlFragment.statusDSP.setText(txt);
        }

        public void setText(String txt) {
            this.txt = txt;
        }

    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("currentFragment", 3);
    }
    public void onPause() {
        super.onPause();
        Bundle b = new Bundle();
        b.putInt("id", 3);
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Bundle b = new Bundle();
        b.putInt("id", 3);
        try {
            if (channelssh.isConnected()) {
                channelssh.disconnect();
                textViewUpdater.setText("camera disconnected reconnect");
            }
        } catch (NullPointerException e) {

        }

    }

    public void onResume() {
        super.onResume();
        Bundle b = new Bundle();
        int id = b.getInt("id");

    }

}