package nm.org.kitcamctrl.listcontent;

import android.os.Handler;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import nm.org.kitcamctrl.CameraListFragment;
import nm.org.kitcamctrl.ItemDatabasehandler;
import nm.org.kitcamctrl.ItemRecord;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class ListAdapterContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<HostItem> ITEMS = new ArrayList<HostItem>();
    public static ArrayList<HashMap<String, String>> itemArrayList = new ArrayList<HashMap<String, String>>();


    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, HostItem> ITEM_MAP = new HashMap<String, HostItem>();

    public static String ITEM_ID; //DATABASE RECORD ID REQUIRED FOR LIST
    private final Handler handler = new Handler();
    static ItemDatabasehandler db;

    static {
        // Add 3 sample items.
       //addItem(new HostItem("1", "Item 1"));
        //addItem(new HostItem("2", "Item 2"));
        //addItem(new HostItem("3", "Item 3"));
      //Context c; //null;
      /* try {
           c.getApplicationContext();
           Log.d("BCG", c.getApplicationContext().toString());
       } catch (NullPointerException e) {
           Log.d("BGC", "CANNOT GET CONTEXT");
       }*/
        db = new ItemDatabasehandler(CameraListFragment.c);
        int value;


        List<ItemRecord> values = db.getAllItemRecords();
        for (ItemRecord cn : values) {
            //String log = "Id: " + cn.getID() + " ,typeCode: " + cn.getTypeCode() + " ,Item: " + cn.getItemData();
            ITEM_ID = Integer.toString(cn.getID());
            //ITEM_TC = cn.getTypeCode();
            //ITEM_DATA = cn.getItemData();
            //Log.d("Name: ", log);
            // creating new HashMap
            HashMap<String, String> map = new HashMap<String, String>();
            // adding each child node to HashMap key => value

            /*map.put("ITEM_ID", ITEM_ID);
            map.put("ITEM_IP", cn.getHostname());
            map.put("ITEM_USER", cn.getUser());
            map.put("ITEM_PW", cn.getPasswd());
            map.put("ITEM_PORT", cn.getPort());
            map.put("ITEM_FRN", cn.getFRN());*/
            addItem(new HostItem(Integer.toString(cn.getID()), cn.getFRN()));
            // adding HashList to ArrayList
            itemArrayList.add(map);
        }
        Log.d("LIST ADP IAL:", ITEMS.toString());



        /*for (ItemRecord cn : values) {
            //String log = "Id: " + cn.getID() + " ,typeCode: " + cn.getTypeCode() + " ,Item: " + cn.getItemData();
            ITEM_ID = Integer.toString(cn.getID());
            //ITEM_TC = cn.getTypeCode();
            //ITEM_DATA = cn.getItemData();
            //Log.d("Name: ", log);
            // creating new HashMap
            HashMap<String, String> map = new HashMap<String, String>();
            // adding each child node to HashMap key => value

            map.put("ITEM_ID", ITEM_ID);
            map.put("ITEM_IP", cn.getHostname());
            map.put("ITEM_USER", cn.getUser());
            map.put("ITEM_PW", cn.getPasswd());
            map.put("ITEM_PORT", cn.getPort());
            map.put("ITEM_FRN", cn.getFRN());
            // adding HashList to ArrayList
            itemArrayList.add(map);*/
        //}
    }

    private static void addItem(HostItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);




    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class HostItem {
        public String id;
        public String content;
        //public Context context;

        public HostItem(String id, String content) { //Context context) {
            this.id = id;
            this.content = content;
           /// this.context =  context;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
